# deploy

- deploy the todo cookiecutter template

```sh
# first time
cookiecutter gl:gitlab.com/shane0/cookiecutter-todocli/
# after using via remote it can then be used locally
cookiecutter cookiecutter-todocli
```
