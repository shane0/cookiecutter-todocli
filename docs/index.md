
!!! info

        - this is a minimal [cookiecutter](https://github.com/cookiecutter/cookiecutter) template for [todocli](https://github.com/francoischalifour/todo-cli) that works using relative alias shortcuts + mkdocs integration

[TAGS]
